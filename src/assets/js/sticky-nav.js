import $ from 'jquery';
import debounce from 'lodash.debounce'

const scrollHandler = (event, triggerPoint, $navBar) => {
			const currentY = event.currentTarget.scrollY

			if (currentY >= triggerPoint) {
				$navBar.addClass('navbar-shadow')

			} else {
				$navBar.removeClass('navbar-shadow')
			}
		}

export default () => {

	$(document).ready(() => {
		const triggerPoint = $('#nav-triggerpoint').offset().top
		const $navBar = $('#nav')


		$(window).on("scroll", debounce((event) => {
			scrollHandler(event, triggerPoint, $navBar)
		}, 50))

	})
	
}