import 'babel-polyfill'
import $ from 'jquery';
import whatInput from 'what-input'
import stickyNav from './sticky-nav'
import carousels from './lib/carousels'
import hamburgerClick from './hamburger-click'
import menuClose from './mobile-menu-close'
import slideToggle from './slide-toggle'
//
import googleAnalytics from './google-analytics'
import hotjar from './hotjar'
import liveHelpNow from './live-help-now'
import conditionalForm from './conditional-form'
import contentLocker from './lib/content-locker'
// import vobRecaptcha from './vob-recaptcha'

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

// Carousel
carousels()

// Sticky Nav
stickyNav()

//Hamburger Click
hamburgerClick()

//Menu Close
menuClose()

// Live Help Now
liveHelpNow()

// Google Analytics
googleAnalytics()


